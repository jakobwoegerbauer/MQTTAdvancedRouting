from mqttAdvancedRouting import mqttclient
from mqttAdvancedRouting.messageListener import logger, routing, config
from mqttAdvancedRouting.routing import Filter, FilterAlgorithm, Route, RouteManager
from mqttAdvancedRouting import filterAlgorithms
import sys

mqttserver = {
    "url": None,
    "port": 1883
}
arglength = len(sys.argv)
if arglength == 2 or arglength == 3:
    mqttserver["url"] = sys.argv[1]
    if arglength == 3:
        mqttserver["port"] = sys.argv[2]
else:
    raise ValueError("Wrong number of arguments. usage: main.py <mqtt-server> [<port>]")

route_manager = RouteManager('system/routes/', 'add', topic_list_routes_command = 'list/get', topic_list_routes_destination = 'list')
#route_manager.use_filter_algorithm(filterAlgorithms.DemoFilter())
route_manager.use_filter_algorithm(filterAlgorithms.TopicFilter())
route_manager.use_filter_algorithm(filterAlgorithms.PropertyFilter())
route_manager.use_filter_algorithm(filterAlgorithms.TimestampFilter())
route_manager.add_route({
	"destination": "/some/[1]/[-1]",	# should redirect to /some/destination/<last part of source topic>
	"if": [{
		"filter": "TopicFilter",
		"topic": "/some/+/topic/#"
}]})
route_manager.add_route({
	"destination": "/some/topic/destination",
	"if": [{
		"filter": "TimestampFilter",
		"date": "2000:05:21 14:30:00",
		"operator": "<"
	}]
})

client = mqttclient.MqttClient(mqttserver["url"],port=mqttserver["port"])
client.use(logger.MessageLogger("/(system/|some/topic/).*"))
client.use(route_manager)
client.use(config.ConfigProvider("get", "system/config", "configfiles/", "config file not found"))
client.use(routing.Setup())

tries = 1
while(True):
    try:
        client.start(True)
        tries = 1
    except Exception as e:
        print str(e)
        print "connect error. reconnecting... (try " + str(tries) + ")"
        tries += 1