# Mqtt Advanced Routing
MQTTAdvancedRouting adds advanced routing functionalities to a MQTT service within a MQTT client.

# Hilfe für Konfigurationsdateien
Um eine Konfigurationsdatei anzufragen, muss eine (leere) Message an
>system/config/\<name>/get

gepublished werden.
Ist dem System eine Konfigurationsdatei mit dem angegebenen Namen bekannt,
so wird dessen Inhalt an
>system/config/\<name>

gepublished. Der Name entspricht dabei jeweils dem Dateinamen ohne Dateiendung.
Um die Datei topics.config zu erhalten, muss an
>system/config/topics/get

gepublished werden. Der Inhalt wird dann an
>system/config/topics

gepublished.

# Hilfe für Routen
## Hinzufügen von Routen
Routen können mit einer MQTT Message hinzugefügt werden.
Diese muss an 
>system/routes/add

gepublished werden und so aussehen:
```json
{
    "destination": "<destination>",
    "if": [<Filter>, <Filter>, ...]
}
```

\<destination> ist dabei das Topic, an das die Message weitergeleitet werden soll,
wenn alle spezifizierten Filter zutreffen. Dieser Parameter wird im Kapitel **Destination**
näher beschrieben.  
Beispiel:
```json
"destination": "innen/eg/wohnzimmer"
```

Folgende Filter stehen zur Verfügung:
* TopicFilter
* PropertyFilter
* TimestampFilter

Ein einfaches Gesamtbeispiel könnte so aussehen:
```json
{
    "destination": "/temperatur/innen",
    "if": [{
        "filter": "TopicFilter",
        "topic": "/innen/+/+/temperatur"
    }]
}
```
Eine Route mit dieser Konfiguration würde alle Messages an /temperatur/innen weiterleiten,
die an eine Topic eines Temperatursensor im Innenraum gepublished werden.  
Die Filter werden später im Einzelnen genauer beschrieben.

## Destination
Mit der Wildcard ```[x]``` kann der x-te Teil der Original-Topic (0-basiert) in der destination
verwendet werden. Mit ```[-x]``` wird der x-te Teil von hinten eingefügt. ```[-0]``` steht z.B.: für
den letzten Teil der Original-Topic.

Beispiel: 
Eine Message mit dem Absender-Topic ```/innen/eg/wohnzimmer/temperatur```
würde durch eine Route mit
```json
"destination": "sensoren/[3]"
```

nach ```sensoren/temperatur``` weitergeleitet werden.

Für diesen Fall wäre jedoch eine andere Variante besser:

Mit
```json
"destination": "sensoren/[-0]"
```

wird der letzte Teil der Topic verwendet. Das Resultat ist in diesem Fall das Gleiche.  
Wäre das Topic ```aussen/garten/temperatur```, so würde die Variante mit ```[3]``` 
```sensoren/[3]``` liefern, da das Topic nur aus 3 Teilen besteht (Die Wildcard ```[3]``` wird nicht ersetzt, wenn die Topic keinen 3. Teil hat).
Bei der Variante mit ```[-0]``` bleibt das Ergebnis gleich, da sich der letzte Teil der Topic nicht verändert hat.

# Filter
## TopicFilter
Mit dem TopicFilter kann, wie der Name schon sagt, nach Topics gefiltert werden.
Dabei gibt es zwei verschiedene Möglichkeiten.

### Topic Filter mit MQTT Wildcards
Ein TopicFilter mit MQTT Wildcards benötigt folgende Konfiguration:
```json
{
	"filter": "TopicFilter",
	"topic": "<mqtt-topic-pattern>"
}
```
Das Topic Pattern ist dabei ein MQTT Topic und kann die Wildcards ```#``` und ```+``` enthalten.
Ein ```+``` steht für eine beliebige Topic-Ebene. Ein ```#``` steht für einen kompletten Subtree und kann somit nur am Ende des Patterns stehen.

Beispiel:
```json
{
	"filter": "TopicFilter",
	"topic": "innen/+/badezimmer/#"
}
```
Die Messages von allen Sensoren in Badezimmern (in allen Stockwerken) werden weitergeleitet.

z.B.:  
>innen/eg/badezimmer/temperatur  

oder  
>innen/og/badezimmer/licht

### Topic Filter mit Regex
Ein TopicFilter mit Regex Pattern benötigt folgende Konfiguration:
```json
{
	"filter": "TopicFilter",
	"regex": "<regex-pattern>"
}
```
Es werden alle Messages weitergeleitet, deren Topic dem Regex Pattern entspricht.

## PropertyFilter
Ein PropertyFilter benötigt folgende Konfiguration:
```json
{
    "filter": "PropertyFilter",
    ["property": "<property>",]
    "operation": "<operation>",
    "value": <value>
}
``` 
Mit ```property``` wird festgelegt, welches Property des Json-Objekts verglichen werden soll.  
Der Standardwert für ```property``` ist ```value```. Das Feld ```property``` ist daher optional.

```value``` beinhaltet den Vergleichswert. Dies kann eine Zahl, ein Zeichen oder eine Zeichenkette sein.

Mit ```operation``` wird der Vergleichsoperator festgelegt.
Folgende Vergleichsoperatoren stehen zur Verfügung:

|Operator|Vergleich|
|---|---|
|=, ==|Gleich|
|<|Kleiner|
|<=|Kleiner oder gleich|
|>|Größer|
|>=|Größer oder gleich|
|!=, <>|Ungleich|

Beispiel:
```json
{
    "filter": "PropertyFilter",
    "operation": ">",
    "value": 22.5
}
``` 
Diesen Filter könnte man verwenden, um zu Messages über zu hohe Zimmertemperaturen auf ein spezielles Topic weiterzuleiten.  
Die vom Sensor gesendete Message dazu könnte so aussehen:
```json
{
    "value": "22.7",
    "unit": "°C",
    "timestamp": "2016:01:01 14:55:37"
}
``` 
Würde der zu vergleichende Wert nicht unter "value" gesendet werden, müsste man den Namen des Properties im Filter mit der Option ```property``` festlegen.

## TimestampFilter
Mit dem TimestampFilter können die Timestamps der Messages verglichen werden. Dazu müssen die Messages ein Property "timestamp" beinhalten,
in dem ein Timestamp im Format ```JJJJ:MM:DD hh:mm:ss``` eingetragen ist.

Ein TimestampFilter benötigt folgende Konfiguration:
```json
{
    "filter": "TimestampFilter",
    "operator": <operator>,
    "date": <date>
}
```

```date``` steht für den Vergleichswert. Dieser muss - wie das Datum in der Message - folgendes Format haben:  
 >```JJJJ:MM:DD hh:mm:ss```

 Als Vergleichsoperator stehen folgende Möglichkeiten zur Auswahl:
 
|Operator|Vergleich|
|---|---|
|<|Davor|
|<=|Davor oder gleich|
|>|Danach|
|>=|Danach oder gleich|

## Bestehende Routen abfragen
Die im System eingetragenen Routen werden auf
>system/routes/list

gepublished.
Die Message ist als Retain-Message gekennzeichnet, und wird bei jeder Subscription gesendet.  
Zum Aktualisieren dieser Informationen muss eine (leere) Message an
>system/routes/list/get

gepublished werden.

## Kombinierte Beispiele
### Alle Innenräume mit zu hoher Temperatursensor
Das Ziel diese Beispiels ist es, alle Messages von Temperatursensoren im Innenraum an eine aussagekräftige Topic zu senden, wenn die Temperatur zu hoch ist.

Wir gehen davon aus, dass alle relevanten Messages folgende Struktur aufweisen:

```json
{
    "value": <value>,
    "unit": <unit>,
    "timestamp": <timestamp>
}
```
Die Topicstruktur sieht wie folgt aus:
```json
(innen/aussen)/<stockwerk>/<raum>/<sensor>
```
Beispiel für eine Topic:
>innen/eg/wohnzimmer/temperatur

Wir gehen von der allgemeinen Vorlage der Konfiguration aus:
```json
{
    "destination": "<destination>",
    "if": [<Filter>, <Filter>, ...]
}
```

Als erstes wollen wir alle Messages auf eine eigene Topic (```ueberwachung/hitze```) weiterleiten.
```json
{
    "destination": "ueberwachung/hitze",
    "if": []
}
```

Als nächstes fügen wir einen TopicFilter hinzu, um nur Messages von Temperatursensoren im Innenraum weiterzuleiten.
Dabei verwenden wir einen TopicFilter mit MQTT Topic Wildcards.
```json
{
    "destination": "ueberwachung/hitze",
    "if": [{
        "filter": "TopicFilter",
        "topic": "innen/+/+/temperatur"
    }]
}
```
Nun werden alle Messages weitergeleitet, deren Topic mit ```innen``` beginnt, mit ```temperatur``` endet
und dazwischen genau zwei weitere Teile hat.

Um die weitergeleiteten Messages später besser interpretieren zu können, sollen Teile der Topics in die neue Topic übernommen werden.  
Die neue Konfiguration sieht jetzt so aus:
```json
{
    "destination": "ueberwachung/hitze/[1][2]",
    "if": [{
        "filter": "TopicFilter",
        "topic": "innen/+/+/temperatur"
    }]
}
```
Nun werden werden bei den Topics der weitergeleiteten Messages Stockwerk und Raum des Sensors hinzugefügt.
Das resultierende Topic könnte zum Beispiel so aussehen:
>ueberwachung/hitze/og/schlafzimmer

Zu guter Letzt fügen wir noch einen PropertyFilter hinzu, um nur die Messages weiterzuleiten, bei denen die Temperatur zu hoch ist.
Die Konfiguration für diesen Filter sieht so aus:
```json
{
    "filter": "PropertyFilter",
    "operation": ">",
    "value": 23
}
```
Zusammengefügt sieht die gesamte Routenkonfiguration so aus:
```json
{
    "destination": "ueberwachung/hitze/[1][2]",
    "if": [{
        "filter": "TopicFilter",
        "topic": "innen/+/+/temperatur"
    },
    {
        "filter": "PropertyFilter",
        "operation": ">",
        "value": 23
    }]
}
```

Um diese Route zu erstellen, publishen wir folgende Message:  
**Topic:**
>system/routes/add

**Message:**
```json
{
    "destination": "ueberwachung/hitze/[1][2]",
    "if": [{
        "filter": "TopicFilter",
        "topic": "innen/+/+/temperatur"
    },
    {
        "filter": "PropertyFilter",
        "operation": ">",
        "value": 23
    }]
}
```

# Run the MQTTAdvancedRouting client on your own machine
## Setup
### Install python packages: (In windows pip can be found under %Python27%\Scripts)
pip install paho-mqtt

## Test
You can use the following command to publish a message:
mosquitto_pub.exe -h iot.eclipse.org -m "JAKOB, WE CAN PUBLISH MESSAGES!" -t MqttAdvancedRouting

# Sonstiges
Git für die Welt!

Copyright &copy; Lammer Simon, Wögerbauer Jakob 2016