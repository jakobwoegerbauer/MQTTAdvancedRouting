import mqttListener
from StringIO import StringIO
from mqttAdvancedRouting import mqttclient as mqttClientModule, jsonParser

class ConfigProvider(mqttListener.MqttListener):
    
    def __init__(self, configTopicSuffix, publishConfigTopic, configDirectory, configNotFoundMessage):
        self.CONFIG_TOPIC_SUFFIX = configTopicSuffix
        self.CONFIG_DIRECTORY = configDirectory
        self.PUBLISH_CONFIG_TOPIC = publishConfigTopic
        self.CONFIG_NOT_FOUND_MESSAGE = configNotFoundMessage

    def on_message(self, mqttClient, topic, payload, qos, retain):
        if topic.startswith(self.PUBLISH_CONFIG_TOPIC) and topic.endswith(self.CONFIG_TOPIC_SUFFIX):
            filename = topic.split('/')[-2] # e.g. system/config/sensors/set -> find 'sensors' 
            if filename is None:
                return
            mqttClient.publish(self.PUBLISH_CONFIG_TOPIC + "/" + filename, self.readConfig(filename), qos=1, retain=True)

    def readConfig(self, filename):
        try:
            with open(self.CONFIG_DIRECTORY + filename + '.config', 'r') as configfile:
                return configfile.read()
        except IOError:
            return self.CONFIG_NOT_FOUND_MESSAGE