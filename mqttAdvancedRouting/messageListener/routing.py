import mqttListener
from mqttAdvancedRouting import mqttclient as mqttClientModule

class Setup(mqttListener.MqttListener):
    def on_connect(self, mqttClient):
        mqttClient.subscribe("#")
        # request config files and route list so that the actual configurations can be published with retain flag
        mqttClient.publish("system/config/sensors/get")
        mqttClient.publish("system/routes/list/get")