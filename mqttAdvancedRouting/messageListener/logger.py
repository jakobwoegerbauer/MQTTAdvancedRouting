from mqttListener import MqttListener;
import re;

class MessageLogger(MqttListener):
    def __init__(self, topic_pattern=".*"):
        self.topic_regex = re.compile(topic_pattern)

    def on_message(self, mqttClient, topic, payload, qos, retain):
        if self.topic_regex.match(topic):
            print(topic+" "+str(payload));