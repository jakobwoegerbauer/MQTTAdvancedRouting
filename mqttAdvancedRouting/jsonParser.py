import json

def parse(text):
    try:
        return json.loads(text)
    except:
        return None

def dump(obj):
    try:
        return json.dumps(obj)
    except:
        return "json dump error for " + str(obj)