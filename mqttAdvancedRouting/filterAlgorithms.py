from mqttAdvancedRouting.routing import FilterAlgorithm
from mqttAdvancedRouting import jsonParser
import re, datetime


class DemoFilter(FilterAlgorithm):
	def accepts_message(self, config, topic, payload, qos, retain):
		print "DemoFilter.accepts_message"
		return config # must return a boolean value

	def parse_config(self, algorithm_config):
		print "DemoFilter.parse_config(" + str(algorithm_config) + ")"
		# return None if the configuration is invalid
		return True # the value returned here is usable in 'accepts_message' via the 'config' param.

class TopicFilter(FilterAlgorithm):
    def accepts_message(self, config, topic, payload, qos, retain):
        return config(topic)

    def parse_config(self, algorithm_config):
        if algorithm_config.has_key("topic"):
            pattern = re.escape(algorithm_config["topic"])
            pattern = pattern.replace("\+", "[^/]*") # replace '+'
            if "#" in pattern:
                pattern = "^" + pattern[0:pattern.index('\#')] + ".*$" # replace '#'
            r = re.compile(pattern)
            if r == None:
                return None
            return lambda topic: r.match(topic)
        elif algorithm_config.has_key("regex"):
            r = re.compile(algorithm_config["regex"])
            if r == None:
                return None
            return lambda topic: r.match(topic)
        else:
            return None

class TimestampFilter(FilterAlgorithm):
    def __init__(self):
        self.operations = {
            "<" : lambda date, referenceDate: date < referenceDate,
            ">" : lambda date, referenceDate: date > referenceDate,
            "<=" : lambda date, referenceDate: date <= referenceDate,
            ">=" : lambda date, referenceDate: date >= referenceDate
        }

    def accepts_message(self, config, topic, payload, qos, retain):
        json = jsonParser.parse(payload)
        if not isinstance(json, dict) or not json.has_key("timestamp"):
            return False
        try:
            date = datetime.datetime.strptime(json["timestamp"], "%Y:%m:%d %H:%M:%S")
        except ValueError:
            return False
        return config["check"](date, config["date"])

    def parse_config(self, algorithm_config):
        if algorithm_config.has_key("operator") and self.operations.has_key(algorithm_config["operator"]) and algorithm_config.has_key("date"):
            try:
                date = datetime.datetime.strptime(algorithm_config["date"], "%Y:%m:%d %H:%M:%S")
            except ValueError:
                return None
            config = {
                "check": self.operations[algorithm_config["operator"]],
                "date": date
            }
            return config
        return None

class PropertyFilter(FilterAlgorithm):
    def __init__(self):
        self.operations = {
            "=" : self.equals,
            "==": self.equals,
            "<" : self.smaller,
            "<=" : self.smaller,
            ">" : self.greater,
            ">=" : self.greaterEquals,
            "!=" : self.notEqual,
            "<>" : self.notEqual
        }

    def equals(self, value, referenceValue):
        return value == referenceValue

    def smaller(self, value, referenceValue):
        return value < referenceValue

    def smallerEquals(self, value, referenceValue):
        return value <= referenceValue
    
    def greater(self, value, referenceValue):
        return value > referenceValue

    def greaterEquals(self, value, referenceValue):
        return value >= referenceValue
    
    def notEqual(self, value, referenceValue):
        return value != referenceValue

    def accepts_message(self, config, topic, payload, qos, retain):
        m = jsonParser.parse(payload)
        if m is None or not isinstance(m, dict) or not m.has_key(config["property"]):
            return False
        return self.operations[config["operation"]](m[config["property"]], config["value"])
    
    def parse_config(self, algorithm_config):
        if not (algorithm_config.has_key("operation") and self.operations.has_key(algorithm_config["operation"]) and algorithm_config.has_key("value")):
            return None
        if not algorithm_config.has_key("property"):
            algorithm_config["property"] = "value"
        return algorithm_config