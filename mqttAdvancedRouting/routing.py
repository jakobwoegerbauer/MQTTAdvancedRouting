import sys
import abc
from StringIO import StringIO
from messageListener import mqttListener
from mqttAdvancedRouting import jsonParser, mqttclient

class FilterAlgorithm:
    __metaclass__ = abc.ABCMeta
    
    # must return a boolean value!
    @abc.abstractmethod
    def accepts_message(self, config, topic, payload, qos, retain):
        pass
    
    @abc.abstractmethod
    def parse_config(self, algorithm_config):
        pass

class Filter:
    def __init__(self, filter_algorithm, config):
        self.initial_config = config
        self.algorithm = filter_algorithm
        self.config = self.algorithm.parse_config(config)
        self.is_valid = (self.config != None)

    def accepts_message(self, topic, payload, qos, retain):
        return self.algorithm.accepts_message(self.config, topic, payload, qos, retain)

    def to_dictionary(self):
        return self.initial_config

class Route:
    def __init__(self, destination, filters = list()):
        self.destination = destination
        self.filters = filters
    
    def accepts_message(self, topic, payload, qos, retain):
        for filter in self.filters:
            if not filter.accepts_message(topic, payload, qos, retain):
                return False
        return True

    def to_dictionary(self):
        filters = []
        for f in self.filters:
            filters.append(f.to_dictionary())
        return {
            "destination": self.destination,
            "filters": filters
        }

class RouteManager(mqttListener.MqttListener):
    def __init__(self, topic_root, topic_add_route, topic_list_routes_command = 'list/get', topic_list_routes_destination = 'list'):
        self.algorithms = dict()
        self.routes = list()
        self.topic_root = topic_root
        self.topic_add_route = topic_add_route
        self.topic_list_routes_command = topic_list_routes_command
        self.topic_list_routes_destination = topic_list_routes_destination

    def use_filter_algorithm(self, algorithm):
        self.algorithms[type(algorithm).__name__] = algorithm
    
    """
    the following message should add a route that forwards messages from the topics '/some/topic/1/' and '/some/topic/2' topic to '/destination'.
    
    /something/routes/add
    {
        destination: "/destination",
        if: [{
                filter: "TopicFilter",
                topic: "/some/topic/1/"
            },{
                filter: "TopicFilter",
                topic: "/some/topic/2/"
            }]
    }
    """
    def add_route(self, config):
        if not config.has_key("destination"):
            print "Cannot add route, because 'destination' was not set!"
            return False
        elif not config.has_key("if"):
            print "Cannot add route, because 'if' was not set!"
            return False
        else:
            filters = list()
            for i in range(len(config["if"])):
                filter_config = config["if"][i]
                if not filter_config.has_key("filter"):
                    print "Cannot add route, because filter with index '" + str(i) + "' was not set!"
                    return False
                filter_algorithm_name = filter_config["filter"]
                if not self.algorithms.has_key(filter_algorithm_name):
                    print "Cannot add route, because filter with index '" + str(i) + "' specified the non-existing algorithm '" + filter_algorithm_name + "'!"
                    return False
                f = Filter(self.algorithms[filter_algorithm_name], filter_config)
                if not f.is_valid:
                    print "Cannot add route, because the configuration of the filter with index '" + str(i) + "' is invalid!"
                    return False
                else:
                    filters.append(f)
            route = Route(config["destination"], filters)
            self.routes.append(route)
            print "Route added"
            return route

    def get_routes_as_text(self):       
        data = []
        for route in self.routes:
            data.append(route.to_dictionary())
        return jsonParser.dump(data)

    def interpret_destination(self, source_topic, destination):
        arr = source_topic.split('/')
        arr = filter(lambda item: item != "", arr)
        for i in range(len(arr)):
            destination = destination.replace('[' + str(i) + ']', arr[i])
            destination = destination.replace('[-' + str(i) + ']', arr[len(arr) - i - 1])
        return destination

    def on_message(self, mqtt_client, topic, payload, qos, retain):
        if topic == self.topic_root + self.topic_add_route:
            route_config = jsonParser.parse(payload)
            if route_config == None:
                print "Cannot add route, because the json message was invalid!"
            else:
                route = self.add_route(route_config)
        if topic == self.topic_root + self.topic_list_routes_command:
            mqtt_client.publish(self.topic_root + self.topic_list_routes_destination, payload=self.get_routes_as_text(), qos=1, retain=True)
        for route in self.routes:
            if route.accepts_message(topic, payload, qos, retain):
                destination = self.interpret_destination(topic, route.destination)
                if destination == topic:
                    return
                print "forward message '" + payload + "' from '" + topic + "' to '" + destination + "'."
                mqtt_client.publish(destination, payload=payload, qos=qos, retain=retain)