import paho.mqtt.client as mqtt
import paho.mqtt.publish as mqttpublish

class MqttClient:
    host = "localhost"
    port = 1883

    def __init__(self,host,port=1883,keepalive=60,bind_address=""):
        MqttClient.host = host;
        MqttClient.port = port;
        self.keepalive = keepalive;
        self.bind_address = bind_address;
        self.listener = [];
        
    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(self,client, userdata, flags, rc):
        print("Connected with result code "+str(rc))
        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        for listener in self.listener:
            listener.on_connect(self);

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self, client, userdata, msg):
        for listener in self.listener:
            listener.on_message(self, msg.topic, msg.payload, msg.qos, msg.retain);

    def start(self, blocking = False, client_id=None, clean_session=True):
        self.client = mqtt.Client(client_id=client_id,clean_session=clean_session)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect

        print "connecting to: " + MqttClient.host + ":" + str(MqttClient.port) + " ..."
        self.client.connect(host=MqttClient.host, port=MqttClient.port, keepalive=self.keepalive, bind_address=self.bind_address)

        # Blocking call that processes network traffic, dispatches callbacks and
        # handles reconnecting.
        # Other loop*() functions are available that give a threaded interface and a
        # manual interface.
        
        if blocking:
            self.client.loop_forever();
        else:
            self.client.loop_start();

    def on_disconnect(self, client, userdata, rc):
        print "disconnected"
        self.client.disconnect() # stop loop

    def subscribe(self, topic):
        self.client.subscribe(topic);

    def stop(self):
        self.client.loop_stop();
        for listener in self.listener:
            listener.on_stop(self);

    def use(self, listener):
        self.listener.append(listener);
    
    def publish(self, topic, payload=None, qos=0, retain=False):
        self.client.publish(topic, payload=payload, qos=qos, retain=retain)

def publish(topic, msg, qos=0, retain=False, will=None, host=MqttClient.host, port=MqttClient.port, clientId="MqttAdvancedRouting"):
    mqttpublish.single(topic, payload=msg, qos=qos, retain=retain, hostname=host, 
    port=port, client_id=clientId, keepalive=60, will=None, auth=None, tls=None, 
    protocol=mqtt.MQTTv311)
